roffit (0.16-1) unstable; urgency=medium

  * New upstream version 0.16
  * d/copyright: Update upstream's range of copyright years.

 -- Sven Geuer <sge@debian.org>  Tue, 18 Jun 2024 21:28:06 +0200

roffit (0.15-2) unstable; urgency=medium

  * d/copyright:
    - Correct the "Files: *" stanza's license to "curl".
    - Change the "Files: debian/*" stanza's license to upstream's license.

 -- Sven Geuer <sge@debian.org>  Sun, 09 Jun 2024 18:58:05 +0200

roffit (0.15-1) unstable; urgency=medium

  * d/watch: Refactor the watch file to make it functional again.
  * New upstream version 0.15
  * d/control:
    - Adopt package setting Maintainer to myself (Closes: #920124).
    - Update Vcs-Browser, Vcs-Git and Homepage.
    - Refactor override_dh_auto_* targets (Closes: #889975).
    - Add libhtml-parser-perl missing from Depends.
    - Bump Standards-Version to 4.7.0.
    - Add Rules-Requires-Root with value "no".
    - Add Multi-Arch with value "foreign".
    - Apply "wrap-and-sort -at".
  * d/t/control: Introduce autopkgtest.
  * d/u/metadata: Add upstream metadata file.
  * d/copyright:
    - Correct the Format field.
    - Update the fields Upstream-Contact and Source.
    - Drop the field X-Upstream-Vcs.
    - Correct and update the "Files: *" stanza.
    - Update the "Files: debian/*" stanza.
    - Update MIT-custom license quoted from file LICENSE.
  * d/salsa-ci.yml: Introduce Salsa-Ci configuration file.
  * d/gbp.conf: Introduce gbp configuration file.

 -- Sven Geuer <sge@debian.org>  Sun, 02 Jun 2024 20:10:54 +0200

roffit (0.7~20120815+gitbbf62e6-2) unstable; urgency=medium

  * QA upload.
  * Set Debian QA Group as maintainer.(see #920124)
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
      Build-Depends field and bumped level to 13.
  * debian/control: bumped Standart-Version to 4.5.0.

 -- Leonardo Rodrigues Pereira <leontecnicalonline@gmail.com>  Tue, 12 May 2020 22:08:51 -0300

roffit (0.7~20120815+gitbbf62e6-1) unstable; urgency=low

  * New upstream snapshot.
    - Perl 5.8.8 fixes.
  * debian/clean
    - New file.
  * debian/control
    - (Build-Depends): Remove dpkg-dev; not needed with debhelper 9.
    - (Standards-Version): 3.9.3.
  * debian/copyright
    - (Format): Update to 1.0.
  * debian/install
    - Rename from <package>.install
    - Install manual page.
  * debian/manpages
    - Rename from <package>.manpages
    - Do not install testpage.1.
      Patch thanks to Ricardo Mones <mones@debian.org>.
  * debian/rules
    - Remove hardened build flags; not needed for a Perl program.
    - (override_dh_auto_config, override_dh_auto_build): Disable rules.
      Patch thanks to Ricardo Mones <mones@debian.org>.

 -- Jari Aalto <jari.aalto@cante.net>  Fri, 17 Aug 2012 15:30:21 +0300

roffit (0.7~20100607+git790d154-3) unstable; urgency=low

  * debian/compat
    - Update to 9
  * debian/control
    - (Build-Depends): update to debhelper 9, dpkg-dev 1.16.1.
  * debian/rules
    - Use hardened CFLAGS.
      http://wiki.debian.org/ReleaseGoals/SecurityHardeningBuildFlags

 -- Jari Aalto <jari.aalto@cante.net>  Sat, 11 Feb 2012 02:00:31 -0500

roffit (0.7~20100607+git790d154-2) unstable; urgency=low

  * debian/control
    - (Build-Depends): Update to debhelper 8.
    - (Description): Improve wording (Closes: #657052).
    - (Standards-Version): Update to 3.9.2.
  * debian/compat
    - Update to 8.
  * debian/copyright
    - Update to DEP5.

 -- Jari Aalto <jari.aalto@cante.net>  Mon, 23 Jan 2012 23:02:21 +0200

roffit (0.7~20100607+git790d154-1) unstable; urgency=low

  * New upstream release (Closes: #584439, #584459, #584437).
  * debian/copyright
    - Add upstream version control location.
  * debian/source/format
    - Update from 1.0 to 3.0

 -- Jari Aalto <jari.aalto@cante.net>  Fri, 11 Jun 2010 14:35:49 +0300

roffit (0.6+cvs20090507-1) unstable; urgency=low

  * Initial release (Closes: #579730).

 -- Jari Aalto <jari.aalto@cante.net>  Fri, 30 Apr 2010 12:39:26 +0300
